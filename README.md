# CSS Grid demo
- simple image gallery using CSS Grid
- live demo at https://jhruby.gitlab.io/grid-ovx/

## Credits
- images: https://unsplash.com
- design: https://dribbble.com/shots/2445515-Rangefinder-V2
